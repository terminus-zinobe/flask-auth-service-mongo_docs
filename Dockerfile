FROM python:3.7
ENV WORKDIR="/var/www/main"
RUN mkdir -p $WORKDIR
WORKDIR $WORKDIR
COPY . $WORKDIR
RUN pip install -r requirements.txt
EXPOSE 8080
RUN make html
CMD [ "python", "-m", "http.server", "8080", "-d", "/var/www/main/build/html/" ]
