# flask-auth-service-mongo_docs

Documentation for flask-auth-service-mongo package.

## Entorno Virtual
- Crear
    ```shell
    $ python3 -m venv venv
    ```
- Activar
    ```shell
    $ source venv/bin/activate
    ```
- Desactivar
    ```shell
    $ deactivate
    ```

## Dependencias
- Instalar dependencias
    ```shell
    $ pip3 install -r requirements.txt
    ```

## HTML
- Crear html
    ```shell
    $ make html
    ```

- Ver html
    ./build/html/index.html

## Run in docker
- Para correr en docker solo corre este comando:
    ```shell
    $ docker-compose up --build
    ```
    - el parametro ¨--build` es solo para la primera vez o cuando hay alguna actualizacion


## Refs:
- [Sphinx](http://www.sphinx-doc.org/en/master/)
- [Theme](https://sphinx-themes.org/html/sphinx-glpi-theme/glpi/index.html)
- [httpdomain](https://sphinxcontrib-httpdomain.readthedocs.io/en/stable/)
