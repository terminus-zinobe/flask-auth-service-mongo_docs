Session
-------

This service provides a Login and Logout.


Login
~~~~~

Check this example to how use the Login

::

    from flask import Blueprint
    from flask_restplus import Api, Resource
    from flask_auth_service_mongo import api_rest

    view_admin = Blueprint('view_admin', __name__)
    api = Api(view_admin)


    @api.route('/login')
    class ApiLogin(Resource):
        def post(self):
            return api_rest.login(role='admin')

.. http:post:: /admin/login

    **Example request**:

    .. sourcecode:: http

        POST /admin/login HTTP/1.1
        Host: example.com
        Content-Type: application/json

        {
            "username": "username",
            "password": "password"
        }

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "message": "ok",
            "data": {
                "change_password": true,
                "token_type": "Bearer",
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODQxMTMzNjUsImlhdCI6MTU4NDEwOTc2NSwic3ViIjoiNWU2NmE5NzZlYmM3NDY5YzZlYjg",
                "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ93OTViIiwidXVpZCI6IjBhNGU1Z",
                "expires_in": 60,
                "role": "role"
            }
        }

    :statuscode 200:

        If **change_password == true** the user needs to change the password.

        **expires_in** time in minutes the token expires.

    **Example response Error**:

    .. sourcecode:: http

        HTTP/1.1 400 BAD REQUEST
        Content-Type: application/json

        {
            "message": "bad_request"
        }


Logout
~~~~~~

Check this example to how use Logout

::

    from flask import Blueprint
    from flask_restplus import Api, Resource
    from flask_auth_service_mongo import api_rest, auth

    view_admin = Blueprint('view_admin', __name__)
    api = Api(view_admin)


   @api.route('/logout')
    class ApiLogout(Resource):
        @auth.required(role='admin')
        def post(self):
            return api_rest.logout()

.. http:post:: /admin/logout

    **Example request**:

    .. sourcecode:: http

        POST /admin/logout HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token_123_xD

        {}

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "message": "ok"
        }

.. note::
    Remember to register your blueprint module in your ``create_app()``.

    Example::

        def create_app():
            app = Flask(__name__)
            ...

            from views.admin import view_admin
            app.register_blueprint(view_admin, url_prefix='/admin')

            return app


Update password
~~~~~~~~~~~~~~~

Update password of current user

.. http:post:: /admin/graphql

    GraphQL: Mutation Reset Password User

    **Example request**:

    .. sourcecode:: http

        POST /admin/graphql HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token_123_xD

    .. code-block:: python

        mutation {
            update_password (input: {
                current_password: "current"
                new_password: "new_pass"
                password_confirmed: "new_pass"
            }) {
                ok
            }
        }

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "data": {
                "update_password": {
                    "ok": true
                }
            }
        }
