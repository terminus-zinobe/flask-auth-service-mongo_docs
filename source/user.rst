CRUD User
---------

This package provides the Create, Read, Update and Delete options.
It's ready to integrate with ``graphene``.


Schema
~~~~~~

A simple example to how to define the schema for ``user``

::

    import graphene
    from graphene.relay import Node
    from graphene_mongo import MongoengineConnectionField
    from flask_auth_service_mongo import schema as auth_schema


    class QueryAdmin(graphene.ObjectType):
        node = Node.Field()
        user = MongoengineConnectionField(auth_schema.User)
        role = MongoengineConnectionField(auth_schema.Role)


    class MutationAdmin(graphene.ObjectType):
        create_user = auth_schema.CreateUser.Field()
        update_user = auth_schema.UpdateUser.Field()
        delete_user = auth_schema.DeleteUser.Field()


    schemaAdmin = graphene.Schema(
        query=QueryAdmin,
        mutation=MutationAdmin,
        auto_camelcase=False
    )


View
~~~~
A simple example to how to define ``view``

::

    from flask import Blueprint
    from flask_graphql import GraphQLView
    from utils.decorators import result_to_json
    from schemas import schemaAdmin


    view_admin = Blueprint('view_admin', __name__)


    view_admin.add_url_rule(
        '/graphql',
        view_func=result_to_json(
            auth.required(role='admin')(
                GraphQLView.as_view(
                    'graphql',
                    schema=schemaAdmin,
                    graphiql=True
                )
            )
        ),
        methods=['GET', 'POST']
    )


Create
~~~~~~

.. http:post:: /admin/graphql

    GraphQL: Mutation create User

    **Example request**:

    .. sourcecode:: http

        POST /admin/graphql HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token_123_xD

    .. code-block:: python

        mutation {
            create_user (input: {
                username: "userAdmin"
                password: "aP#D.o"
                password_confirmed: "aP#D.o"
                role: "admin"
            }) {
                user {
                    id
                }
            }
        }

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "data": {
                "create_user": {
                    "user": {
                        "id": "VXNlcjo1ZTBhNjFkYmFmYzhhNDI4MWIyMmM2ZGY="
                    }
                }
            }
        }


Read
~~~~

.. http:get:: /admin/graphql

    GraphQL: Query Users

    **Example request**:

    .. sourcecode:: http

        GET /admin/graphql HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token_123_xD

    .. code-block:: python

        {
            user {
                edges {
                    node {
                        id
                        username
                        change_password
                        role {
                            name
                        }
                    }
                }
            }
        }

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "data": {
                "user": {
                    "edges": [
                        {
                            "node": {
                                "id": "VXNlcjo1ZTMzMzUyNWYxNzBjZmM5MmNkYTgwYmE=",
                                "username": "userAdmin",
                                "change_password": false,
                                "role": {
                                    "name": "admin"
                                }
                            }
                        }
                    ]
                }
            }
        }


Update
~~~~~~

.. http:post:: /admin/graphql

    GraphQL: Mutation update User

    **Example request**:

    .. sourcecode:: http

        POST /admin/graphql HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token

    .. code-block:: python

        mutation {
            update_user (input: {
                id: "VXNlcjo1ZTY2OWNlMWZkMzRlMTJmMjQwMjk5OTk="
                change_password: false
            }) {
                user {
                    id
                    change_password
                }
            }
        }

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "data": {
                "update_user": {
                    "user": {
                        "id": "VXNlcjo1ZTY2OWNlMWZkMzRlMTJmMjQwMjk5OTk=",
                        "change_password": false
                    }
                }
            }
        }


Delete
~~~~~~

.. http:post:: /admin/graphql

    GraphQL: Mutation delete User

    **Example request**:

    .. sourcecode:: http

        POST /admin/graphql HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token_123_xD

    .. code-block:: python

        mutation {
            delete_user (input: {
                id: "VXNlcjo1ZTBhNjNlOWFmYzhhNDI4MWIyMmM2ZTA="
            }) {
                ok
            }
        }

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "data": {
                "delete_user": {
                    "ok": true
                }
            }
        }


Reset password
~~~~~~~~~~~~~~

Generate a random password for the user

.. http:post:: /admin/graphql

    GraphQL: Mutation Reset Password User

    **Example request**:

    .. sourcecode:: http

        POST /admin/graphql HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token_123_xD

    .. code-block:: python

        mutation {
            reset_password (input: {
                id: "VXNlcjo1ZTQwNDY1M2QxYzA5OGM1MTc3Y2M2OWQ="
            }) {
                password
            }
        }

    **Example response Ok**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "data": {
                "reset_password": {
                    "password": "abcd"
                }
            }
        }
