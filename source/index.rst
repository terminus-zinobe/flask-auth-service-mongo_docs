Welcome to Flask-Auth-Service-Mongo's documentation!
====================================================

Flask Auth Service Mongo is an Auth JWT implementation for Flask_ with Mongo_.
It provides useful services such as: Authentication, CRUD for User and Role managment.

.. _Flask: https://palletsprojects.com/p/flask/
.. _Mongo: https://www.mongodb.com/

Get the code
------------

The `source <https://gitlab.com/terminus-zinobe/flask-auth-service-mongo>`_ is available on GitLab. 

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:

    install
    configuration
    userguide



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
