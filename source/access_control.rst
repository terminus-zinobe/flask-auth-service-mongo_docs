Access Control
--------------

This service provides a access control.


Paths
~~~~~

Check this example to how use auth.required.


is_authenticated
````````````````

::

    from flask import Blueprint
    from flask_restplus import Api, Resource
    from flask_auth_service_mongo import auth

    view_admin = Blueprint('view_admin', __name__)
    api = Api(view_admin)


    @api.route('/blog')
    class ApiLogout(Resource):
        @auth.required()
        def post(self):
            return {
                'message': 'Ok'
            }

.. http:post:: /admin/blog

    **Example request**:

    .. sourcecode:: http

        POST /admin/blog HTTP/1.1
        Host: example.com
        Content-Type: application/json
        Authorization: Bearer token_123_xD

        {}


specific_role
`````````````

::

    from flask import Blueprint
    from flask_restplus import Api, Resource
    from flask_auth_service_mongo import auth

    view_admin = Blueprint('view_admin', __name__)
    api = Api(view_admin)


    @api.route('/blog')
    class ApiLogout(Resource):
        @auth.required('custom_role')
        def post(self):
            return {
                'message': 'Ok'
            }


list_roles
``````````

::

    from flask import Blueprint
    from flask_restplus import Api, Resource
    from flask_auth_service_mongo import auth

    view_admin = Blueprint('view_admin', __name__)
    api = Api(view_admin)


    @api.route('/blog')
    class ApiLogout(Resource):
        @auth.required(['custom_role', 'other_role'])
        def post(self):
            return {
                'message': 'Ok'
            }


Middleware Mutations Graphene
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Check this example to how use MutationMiddleware.

::

    import graphene
    from flask import Blueprint
    from flask_auth_service_mongos import MutationMiddleware

    view_admin = Blueprint('view_admin', __name__)

    my_schema = graphene.Schema()

    my_access_control = [
        {'mutation': 'create_blog', 'roles': ['is_authenticated']},
        {'mutation': 'update_blog', 'roles': ['role_user', 'role_admin']},
        {'mutation': 'delete_blog', 'roles': ['role_admin']},
    ]

    view_admin.add_url_rule(
        '/graphql',
        view_func=result_to_json(
            auth.required(role='admin')(
                GraphQLView.as_view(
                    'graphql',
                    schema=my_schema,
                    graphiql=True,
                    middleware=[
                        MutationMiddleware(my_access_control)
                    ]
                )
            )
        ),
        methods=['GET', 'POST']
    )
