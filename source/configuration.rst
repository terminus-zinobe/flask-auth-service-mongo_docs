Configuration
=============

ENV vars
--------

Define the following environment variables

- (str) Key with which the token is generated
    ``SECRET_KEY=``

- (bool) Turn the token whitelist on or off
    ``WHITE_LIST_TOKEN=``

- (int) Minimum username length
    ``USERNAME_MIN_LENGTH=``

- (int) Minimum password length
    ``PASSWORD_MIN_LENGTH=``

- (int) Minutes in which the token will expire
    ``TOKEN_EXPIRE_MINUTES=``

- (int) Length of the password generated in the reset
    ``RESET_PASSWORD_LEN_GENERATOR=``

- (bool) Default value for auth.required(require_password_change)
    ``REQUIRE_PASSWORD_CHANGE=``


Example
```````
    ::

        USERNAME_MIN_LENGTH=5
        PASSWORD_MIN_LENGTH=8
        WHITE_LIST_TOKEN=0
        SECRET_KEY='not-secret'
