Commands
--------

This service provides commands.


Register module
~~~~~~~~~~~~~~~

Register the blueprint module in your ``create_app()``.

Example::

    def create_app():
        app = Flask(__name__)
        ...

        from flask_auth_service_mongo import command_auth_mongo
        app.register_blueprint(command_auth_mongo)

        return app


All commands
~~~~~~~~~~~~

Show all available commands::

    flask flask_auth --help


Create Role
~~~~~~~~~~~

Create a Role::

    flask flask_auth role-new --help


Create User
~~~~~~~~~~~

Create a User::

    flask flask_auth user-new --help


Clear tokens
~~~~~~~~~~~~

Clear all tokens of the WhitelistToken::

    flask flask_auth clear-tokens --help
